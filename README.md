# File manager - HD module Yii2.
====================


## Installation

Edit your `composer.json` and add:

```json
{
	"require": {

            "hdmodules/file": "*"

	}
}
```

Don't forget add repository:

```json
{
	"repositories": {
            {
                "type": "vcs",
                "url":  "https://gitlab.qazz.pw/hdmodules/file.git"
            }
	}
}
```


## Basic setup

Add the following in your config:

```php
<?php

...

    'modules' => [

        'file' => 'hdmodules\file\FileModule',

        ...
    ]
```


## That's all. Enjoy.








