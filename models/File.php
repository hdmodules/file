<?php
namespace hdmodules\file\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use hdmodules\base\behaviors\SeoBehavior;
use hdmodules\base\behaviors\SortableModel;
use hdmodules\base\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class File extends ActiveRecord
{
    public static function tableName()
    {
        return 'file';
    }
    
    public function rules()
    {
        return [
            ['file', 'file'],
            ['title', 'required'],
            ['title', 'string', 'max' => 128],
            ['title', 'trim'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique', 'targetClass' => '\hdmodules\file\models\File', 'message' => 'This slug exist', 'on' => 'create'],
            [['downloads', 'size'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('easyii', 'Title'),
            'file' => Yii::t('easyii', 'File'),
            'slug' => Yii::t('easyii', 'Slug')
        ];
    }

    public function behaviors()
    {
        return [
            SortableModel::className(),
            'seoBehavior' => SeoBehavior::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true
            ],
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $filePath = Yii::getAlias('@frontend/web') . $this->file;
        file_exists($filePath) ? unlink($filePath) : false;
    }
}