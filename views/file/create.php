<?php

use yii\helpers\Html;

$this->title = 'Create file';

$this->params['breadcrumbs'][] = ['label' => 'Files', ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-10">
        <?=
        yii\widgets\Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Home',
                'url' => Yii::$app->urlManager->baseUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
        ?>
    </div>
    <div class="col-md-2" align="right">
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', ['model' => $model]) ?>
    </div>
</div>
