<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Files';
$this->params['breadcrumbs'][] = $this->title;

$module = $this->context->module->id;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-10">
        <?=
        yii\widgets\Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Home',
                'url' => Yii::$app->urlManager->baseUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
        ?>
    </div>
    <div class="col-md-2" align="right">
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
<?php \yii\widgets\Pjax::begin(['id' => 'files-form']); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'contentOptions' => ['width' => '3%']
                ],
                [
                    'attribute' => 'id',
                    'contentOptions' => ['width' => '3%'],
                ],
                [
                    'attribute' => 'title'
                ],
                [
                    'attribute' => 'file'
                ],
                [
                    'attribute' => 'slug',
                    'contentOptions' => ['width' => '10%'],
                ],
                [
                    'attribute' => 'downloads',
                    'contentOptions' => ['width' => '5%'],
                ],
                [
                    'attribute' => 'size',
                    'contentOptions' => ['width' => '10%'],
                    'value' => function($data) {
                        return Yii::$app->formatter->asShortSize($data->size, 2);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['width' => '5%', 'align' => 'center'],
                    'template' => '{up} {down}',
                    'buttons' => [
                        'up' => function ($url, $model) {
                            return Html::a('<span class="fa fa-fw fa-arrow-up"></span>', ['up', 'id' => $model->id]);
                        },
                        'down' => function ($url, $model) {
                            return Html::a('<span class="fa fa-fw fa-arrow-down"></span>', ['down', 'id' => $model->id]);
                        },
                    ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['width' => '3%'],
                    'template' => '{update} {delete}'
                ],
            ],
        ]);
                ?>
<?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
