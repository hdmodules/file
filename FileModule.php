<?php
namespace hdmodules\file;

use hdmodules\base\components\Module;

class FileModule extends Module
{
    public static $installConfig = [
        'icon' => 'floppy-disk',
        'order_num' => 30,
    ];
}