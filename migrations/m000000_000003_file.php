<?php

use yii\db\Migration;
use yii\db\Schema;

class m000000_000003_file extends Migration
{

    public function safeUp()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(128),
            'file' => $this->string(255),
            'size' => $this->integer()->notNull(),
            'slug' => $this->string(128),
            'downloads' => $this->integer()->defaultValue(0),
            'create_time'=> $this->integer()->notNull(),
            'update_time'=> $this->integer(),       
            'order_num' => $this->integer()->defaultValue(0),
        ],  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' );
        
        $this->createTable('file_relation', [
            'id' => $this->primaryKey(11),
            'file_id' => $this->integer()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'table_name' => $this->string(50)
        ],  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' );

        $this->createIndex(
            'file_id',
            'file_relation',
            'file_id'
        );
        
        $this->addForeignKey(
            'fk-file_id',
            'file_relation',
            'file_id',
            'file',
            'id',
            'CASCADE'
        );
    }

    public function safeDowndown()
    {   
        $this->dropTable('file_relation');
        $this->dropTable('file');
        
        echo "m000000_000000_file was deleted.\n";
         
        return true;
    }

}
