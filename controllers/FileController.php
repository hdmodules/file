<?php

namespace hdmodules\file\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use hdmodules\base\controllers\Controller;
use hdmodules\file\models\File;
use hdmodules\base\helpers\Upload;
use hdmodules\base\controllers\SortableController;

class FileController extends Controller {

    public function behaviors() {
        return [
            [
                'class' => SortableController::className(),
                'model' => File::className()
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => File::find()->sort(),
        ]);
        return $this->render('index', [
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate($slug = null) {
        $model = new File;
        $model->scenario = 'create';
        
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if (($fileInstanse = UploadedFile::getInstance($model, 'file'))) {
                    $model->file = $fileInstanse;
                    if ($model->validate(['file'])) {

                        $model->file = Upload::file($fileInstanse, 'files', true);
                        $model->size = $fileInstanse->size;

                        if ($model->save()) {
                            $this->flash('success', Yii::t('easyii/file', 'File created'));
                            return $this->redirect(['index']);
                        } else {
                            $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                        }
                    } else {
                        $this->flash('error', Yii::t('easyii/file', 'File error. {0}', $model->formatErrors()));
                    }
                } else {
                    $this->flash('error', Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => $model->getAttributeLabel('file')]));
                }
            }
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = File::findOne($id);

        if ($model === null) {
            $this->flash('error', Yii::t('easyii', 'Not found'));
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if (($fileInstanse = UploadedFile::getInstance($model, 'file'))) {
                    $model->file = $fileInstanse;
                    if ($model->validate(['file'])) {
                        $model->file = Upload::file($fileInstanse, 'files', false);
                        $model->size = $fileInstanse->size;
                    } else {
                        $this->flash('error', Yii::t('easyii/file', 'File error. {0}', $model->formatErrors()));
                    }
                }

                if ($model->save()) {
                    $this->flash('success', Yii::t('easyii/file', 'File updated'));
                    return $this->redirect(['index']);
                } else {
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                }
            }
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete($id) {
        if (($model = File::findOne($id))) {
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/file', 'File deleted'));
    }

    public function actionUp($id) {
        return $this->move($id, 'up');
    }

    public function actionDown($id) {
        return $this->move($id, 'down');
    }

}
